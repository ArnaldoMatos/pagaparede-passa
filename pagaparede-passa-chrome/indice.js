"use strict";

function carregarJSON(jornais, bolachas_urls, referenciador_urls, referenciador, ligacoes_bloqueadas, comandos_a_correr, comandos_a_correr_urls) {
    for (let pais in jornais) {
        if (jornais.hasOwnProperty(pais)) {
            for (let jornal in jornais[pais]) {
                if (jornais[pais].hasOwnProperty(jornal)) {
                    if (jornais[pais][jornal].hasOwnProperty("referenciador")) {
                        referenciador_urls.push("*://*." + jornal + "/*");
                        referenciador_urls.push("*://" + jornal + "/*");

                        referenciador[jornal] = jornais[pais][jornal]["referenciador"];
                    }

                    if (jornais[pais][jornal].hasOwnProperty("bolachas")) {
                        bolachas_urls.push("*://*." + jornal + "/*");
                        bolachas_urls.push("*://" + jornal + "/*");
                        for (let bolacha_url in jornais[pais][jornal]["bolachas"]) {
                            if (jornais[pais][jornal]["bolachas"].hasOwnProperty(bolacha_url)) {
                                bolachas_urls.push("*://*." + bolacha_url + "/*");

                                let c_array = [];
                                for (let i = 0; i < jornais[pais][jornal]["bolachas"][bolacha_url].length; i++) {
                                    c_array.push(jornais[pais][jornal]["bolachas"][bolacha_url][i]);
                                }
                                bolachas[bolacha_url] = c_array;
                            }
                        }
                    }

                    if (jornais[pais][jornal].hasOwnProperty("loading_bloqueado")) {
                        ligacoes_bloqueadas.push("*://*." + jornais[pais][jornal]["loading_bloqueado"]);
                    }

                    if (jornais[pais][jornal].hasOwnProperty("correr_comando")) {
                        // Split para escapar '.' no url
                        comandos_a_correr_urls.push("*://*." + jornal + "/*");
                        comandos_a_correr[jornal.split('\.')[0] + "\." + jornal.split('\.')[1]] = jornais[pais][jornal]["correr_comando"];
                    }
                }
            }
        }
    }
}

function referenciadorEscutador(referenciador_url, bolachas_urls) {
    chrome.webRequest.onBeforeSendHeaders.addListener(
        function (detalhes) {
            //chrome.storage.local.get("referenciador", function (referenciador) {
                //referenciador = referenciador["referenciador"];

            let anfitriao = new URL(detalhes.url).hostname;
            if ((anfitriao.match(/\./g) || []).length > 1) {
                anfitriao = anfitriao.substr(anfitriao.indexOf('.') + 1);
            }
            let cabecalhos = detalhes.requestHeaders;
            console.log(anfitriao);
            console.log(referenciador);
            console.log(referenciador.hasOwnProperty(anfitriao));


            if(referenciador.hasOwnProperty(anfitriao)) {
                let ref_encontrado = false; let agente_utilizador_encontrado = false;

                for (let i = 0; i < cabecalhos.length; ++i) {
                    if (detalhes.requestHeaders[i].name == 'User-Agent') {
                        //detalhes.requestHeaders[i].value = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';
                        //console.log(referenciador[anfitriao]["agente-utilizador"]);
                        detalhes.requestHeaders[i].value = referenciador[anfitriao]["agente-utilizador"];
                        agente_utilizador_encontrado = true;
                    }
                    if (detalhes.requestHeaders[i].name == 'Referer') {
                        //detalhes.requestHeaders[i].value = 'https://www.google.com/';
                        //console.log(referenciador[anfitriao]["referenciador"]);
                        detalhes.requestHeaders[i].value = referenciador[anfitriao]["referenciador"];
                        ref_encontrado = true;
                    }
                }

                if (!ref_encontrado) {
                    //console.log( referenciador[anfitriao]["referenciador"]);
                    cabecalhos.push({
                        "name": "Referer",
                        "value": referenciador[anfitriao]["referenciador"]
                    })
                }
                if (!agente_utilizador_encontrado) {
                    //console.log(referenciador[anfitriao]["agente-utilizador"]);
                    cabecalhos.push({
                        "name": "User-Agent",
                        "value": referenciador[anfitriao]["agente-utilizador"]
                    })
                }
            }


            if (bolachas.hasOwnProperty(anfitriao)) {
                cabecalhos = cabecalhos.filter(function (header) {
                    // block cookies by default
                    if (header.name !== "Cookie") {
                        return header;
                    }
                });
            }

            return {
                requestHeaders: cabecalhos
            };
            //});
        },
        {
            urls: referenciador_url.concat(bolachas_urls)
        },
        [
            'blocking',
            'requestHeaders'
        ]
    );
}

function ligacoesBloqueadasEscutador(ligacoes_bloqueadas) {
    chrome.webRequest.onBeforeRequest.addListener(
        function (detalhes) {
//            console.log("LIGACOES BLOQUEADAS");
//            console.log(detalhes.url);
            //return {cancel: ligacoes_bloqueadas.includes(detalhes.url)};
            return {cancel: true};
        },
        {urls: ligacoes_bloqueadas},
        ["blocking"]
    );
}

function comandosACorrerEscutador(comandos_a_correr_urls) {
    chrome.webNavigation.onCompleted.addListener(function (detalhes) {
            chrome.storage.local.get("comandos_a_correr", function (comandos_a_correr) {
                comandos_a_correr = comandos_a_correr["comandos_a_correr"];
                let anfitriao = new URL(detalhes.url).hostname;
                if ((anfitriao.match(/\./g) || []).length > 1) {
                    anfitriao = anfitriao.substr(anfitriao.indexOf('.') + 1);
                }

                for (let jornal in comandos_a_correr) {
                    if (anfitriao == jornal && comandos_a_correr.hasOwnProperty(jornal)) {
//                        console.log("COMANDOS A CORRER");
//                        console.log("Host:" + anfitriao);
//                        console.log("Jornal:" + jornal);
//                        console.log("Codigo:" + comandos_a_correr[jornal]);
                        chrome.tabs.executeScript(detalhes.tabId, {
                                code: comandos_a_correr[jornal]
                            }
                        );
                    }
                }
            });

        },
        {
            urls: comandos_a_correr_urls
        }
    );
}

function bolachasEscutadorSetCookie(bolachas_urls) {
    chrome.webRequest.onHeadersReceived.addListener(function (detalhes) {
            let anfitriao = new URL(detalhes.url).hostname;
            if ((anfitriao.match(/\./g) || []).length > 1) {
                anfitriao = anfitriao.substr(anfitriao.indexOf('.') + 1);
            }

            if (bolachas.hasOwnProperty(anfitriao)) {
                for (let i = 0; i < detalhes.responseHeaders.length; i++) {
                    if (detalhes.responseHeaders[i].name === "Set-Cookie") {
                        const nome_bolacha = detalhes.responseHeaders[i].value.substr(0, detalhes.responseHeaders[i].value.indexOf('='));
                        if (bolachas[anfitriao].includes(nome_bolacha) || bolachas[anfitriao] == true) {
                            //console.log("BOLACHAS SET");
                            //console.log("Nome Bolacha: " + nome_bolacha);
                            //console.log("Anfitriao: " + anfitriao);
                            //console.log("Pedido: ");
                            //console.log(detalhes.responseHeaders[i]);
                            if (bolachas[anfitriao].includes(nome_bolacha) || bolachas[anfitriao] == true) {
//                                console.log("BOLACHAS SET");
//                                console.log("Nome Bolacha: " + nome_bolacha);
//                                console.log("Anfitriao: " + anfitriao);
//                                console.log("Pedido: ");
//                                console.log(detalhes.responseHeaders[i]);
                                detalhes.responseHeaders.splice(i, 1);
                            }
                        }
                    }
                }
            }
            return {responseHeaders: detalhes.responseHeaders};
        },
        {
            urls: bolachas_urls,
        },
        ["responseHeaders", "blocking"]
    );
}

let bolachas = {};
let referenciador = {};

let xhr = new XMLHttpRequest();
xhr.open("GET", "/data/jornais.json", true);
xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
        let bolachas_urls = [];
        let referenciador_urls = [];

        let ligacoes_bloqueadas = [];
        let comandos_a_correr = {};
        let comandos_a_correr_urls = [];

        let jornais = JSON.parse(xhr.responseText);
        carregarJSON(jornais, bolachas_urls, referenciador_urls, referenciador, ligacoes_bloqueadas, comandos_a_correr, comandos_a_correr_urls);

        //console.log(referenciador_urls);
        chrome.storage.local.set({"comandos_a_correr": comandos_a_correr});
        //chrome.storage.local.set({"referenciador": referenciador});

        referenciadorEscutador(referenciador_urls, bolachas_urls);
        ligacoesBloqueadasEscutador(ligacoes_bloqueadas);

        comandosACorrerEscutador(comandos_a_correr_urls);
        bolachasEscutadorSetCookie(bolachas_urls);
    }
};
xhr.send();


// Cemiterio de tentativas

//"bolachas": { "https://www.washingtonpost.com":["rpld1", "rplmct", "rplm2", "rpld0", "washpost_poe", "s_vi", "s", "v", "vt", "washingtonpost.com_pageview"] }
