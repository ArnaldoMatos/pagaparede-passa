var {Cc, Ci, Cr} = require('chrome');
var data = require('sdk/self').data;
var paginaMod = require("sdk/page-mod");

var bolachasPermissaoServico = Cc["@mozilla.org/cookie/permission;1"].getService(Ci.nsICookiePermission);
var observadorServico = Cc['@mozilla.org/observer-service;1'].getService(Ci.nsIObserverService);
var ioServico = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);

function carregarJSON(jornais, bolachas, referenciador, ligacoes_bloqueadas, comandos_a_correr) {
    for (var pais in jornais) {
        if(jornais.hasOwnProperty(pais)) {
            for(var jornal in jornais[pais]) {
                if (jornais[pais].hasOwnProperty(jornal)) {
                    // Só bloquear bolachas se mudança de referenciador não funcionar
                    if (jornais[pais][jornal]["referenciador"]) {
                        referenciador.push(jornal);
                    } else if (jornais[pais][jornal]["bolachas"]) {
                        bolachas.push(jornal);
                    }

                    if (jornais[pais][jornal].hasOwnProperty("loading_bloqueado")) {
                        ligacoes_bloqueadas.push(jornais[pais][jornal]["loading_bloqueado"]);
                    }

                    if (jornais[pais][jornal].hasOwnProperty("correr_comando")) {
                        // Split para escapar '.' no url
                        comandos_a_correr[".*" + jornal.split('\.')[0] + "\." + jornal.split('\.')[1] + ".*"] = jornais[pais][jornal]["correr_comando"];
                    }
                }
            }
        }
    }
}

function bloquearBolachas(bolachas, bolachasPermissaoServico) {
    for(var i = 0; i < bolachas.length; i++) {
        var uri = ioServico.newURI("http://www." + bolachas[i], null, null);
        var uriHttps = ioServico.newURI("https://www." + bolachas[i], null, null);
        bolachasPermissaoServico.setAccess(uri, bolachasPermissaoServico.ACCESS_DENY);
        bolachasPermissaoServico.setAccess(uriHttps, bolachasPermissaoServico.ACCESS_DENY);
    }
}

function inicializarComandosACorrer(comandos_a_correr, paginaMod) {
    for (var jornal_regex in comandos_a_correr) {
        paginaMod.PageMod({
            include: new RegExp(jornal_regex),
            contentScript: comandos_a_correr[jornal_regex]
        });
    }
}

var jornais = require('./data/jornais.json');

var bolachas = [];
var referenciador = [] ;
var ligacoes_bloqueadas = [];
var comandos_a_correr = {};

carregarJSON(jornais, bolachas, referenciador, ligacoes_bloqueadas, comandos_a_correr);
bloquearBolachas(bolachas, bolachasPermissaoServico);
inicializarComandosACorrer(comandos_a_correr, paginaMod);

var observador = {
    observe: function(sujeito, topico, data) {
        if (topico == 'http-on-modify-request') {
            var channel = sujeito.QueryInterface(Ci.nsIHttpChannel);

            // Verificar se tem www. ou m. antes do url
            if (referenciador.includes(channel.originalURI.host) ||
                referenciador.includes(channel.originalURI.host.split('www.')[1]) ||
                referenciador.includes(channel.originalURI.host.split('m.')[1])) {
                channel.setRequestHeader('User-Agent', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', false);
                channel.setRequestHeader('Referer', 'https://www.google.com', false);
            }

            for(var i = 0; i < ligacoes_bloqueadas.length; i++) {
                if(new RegExp(ligacoes_bloqueadas[i]).test(channel.originalURI.host + channel.originalURI.path)) {
                    /*console.log("BLOQUEADO");
                    console.log(channel.originalURI.host + channel.originalURI.path);
                    console.log(ligacoes_bloqueadas[i]);*/
                    sujeito.cancel(Cr.NS_BINDING_ABORTED);
                }
            }
        }
    }
};

observadorServico.addObserver(observador, 'http-on-modify-request', false);